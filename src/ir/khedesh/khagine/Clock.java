/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ir.khedesh.khagine;

/**
 *
 * @author mohammad
 */
public class Clock {
    
    private int time = 0;
   
    public int step() {
        return ++ time;
    }
    
    public int step(int interval) {
        time += interval;
        return time;
    }
    
    public int get() {
        return time;
    }
    
}
