/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ir.khedesh.khagine;

import java.util.ArrayList;

/**
 *
 * @author mohammad
 */
public abstract class Event {
    
    private int time;
    
    private ArrayList<Entity> required = new ArrayList<>();
    
    public Event(int time) {
        this.time = time;
    }
    
    abstract void action();

    public int getTime() {
        return time;
    }
    
}
