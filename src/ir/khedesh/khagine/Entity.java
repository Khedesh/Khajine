/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ir.khedesh.khagine;

import java.util.ArrayList;

/**
 *
 * @author mohammad
 */
public abstract class Entity {
    
    private static ArrayList< Class<? extends Event> > acceptedEvent = new ArrayList<>();
    
    public static void acceptEvent(Class<? extends Event> eventClass) {
        acceptedEvent.add(eventClass);
    }
    
    public static boolean isAccepted(Class<? extends Event> eventClass) {
        return acceptedEvent.contains(eventClass);
    }
}
