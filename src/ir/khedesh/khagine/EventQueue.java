/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ir.khedesh.khagine;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 *
 * @author mohammad
 */
public class EventQueue extends PriorityQueue<Event> {
    
    private static  Comparator<Event> comparator = new Comparator<Event>() {

        @Override
        public int compare(Event o1, Event o2) {
            return o1.getTime() - o2.getTime();
        }
        
    };
    
    EventQueue() {
        super(comparator);
    }
    
    /* TEMPORARY TEST
    public static void main(String[] argv) {
        EventQueue myQueue = new EventQueue();
        
         myQueue.add(new Event(10));
         myQueue.add(new Event(20));
         myQueue.add(new Event(15));
         
         System.out.println(myQueue.poll().getTime());
        
    }
    */
    
}
